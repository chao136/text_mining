# @Time : 2021/3/27 16:05
# @Author : chao

#读取模型
import gensim
out_model = r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\word2vec\数据\corpusSegDone_zong.model'
model = gensim.models.Word2Vec.load(out_model)
#查看某个字词的向量：
print(model['态度'])
#查看与该词最接近的其他词汇及相似度：
print(model.most_similar(['服务']))
#查看两个词之间的相似度：
print(model.similarity('服务','差'))
