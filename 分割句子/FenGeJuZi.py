# @Time : 2021/3/29 21:37
# @Author : chao

#分割出句子
#使用标点符号分割句子
import re


def fenGe():
    #用来分割句子的标点符号
    biaoDian = [',', '，', '.', '。', '!', '?', ';', '；', '……']

    #构建存储分割后句子的列表
    data = []

    #读取测试数据
    #file = open(r'C:\Users\鲍超\Desktop\测试数据.txt','r', encoding='ANSI')
    #读取原始数据
    file = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\数据\预处理后数据\zong_data.txt',
                encoding='ANSI')
    #写出文件
    out_file = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\分割句子\数据\data_fenge.txt',
                    'w', encoding='utf-8')
    pattern = r',|\.|/|;|\'|`|\[|\]|<|>|\?|:|"|\{|\}|\~|!|@|#|\$|%|\^|&|\(|\)|-|=|\_|\+|，|。|、|；|‘|’|【|】|·|！| |…|（|）'
    for i in file.readlines():
        i = i.strip()
        result_list = re.split(pattern, i)
        for j in result_list:
            data.append(j)

    #删除分句后，出现的空格数据
    while '' in data:
        data.remove('')

    for line in data:
        out_file.write(line + '\n')
    out_file.close()
    file.close()
if __name__ == '__main__':
    fenGe()