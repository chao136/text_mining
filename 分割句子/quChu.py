# @Time : 2021/3/30 9:36
# @Author : chao

#对已经分割好的句子，去除不包含特征名词的句子

#读取特征名词
teZheng_data = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\词性标注\名词提取\名词提取数据\高频名词（过滤后）40词.txt',
                      'r',encoding='ANSI')
#去除名词中的换行符，并保存至teZheng_List中
teZheng_List = []
for line in teZheng_data.readlines():
    teZheng_List.append(line.strip('\n'))

#读取分割好的句子
fenGe_data = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\分割句子\数据\data_fenge.txt',
                  'r', encoding='utf-8')
#将读取的句子保存到fenGe_List 中 并去除换行符
fenGe_List = []
for line in fenGe_data.readlines():
    fenGe_List.append(line.strip('\n'))
print('原始句子共有' + str(len(fenGe_List)) + '条')

#判断句子是否包含特征词，并将包含特征词的句子保存到result_List中
result_List = []
for line in fenGe_List:
    for i in teZheng_List:
        if(i in line):
            result_List.append(line)
            break

print('包含特征词的句子共有：' + str(len(result_List)) + '条')

#保存分割后的含有特征词的句子
out_file = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\分割句子\数据\data_fenge_include_tezheng.txt',
                'w', encoding='utf-8')
for i in result_List:
    out_file.write(i + '\n')
print('输出完成')
